﻿Function Get-FacebookFriends {
        param(
            [string]$URL,
            [string]$Of 
        )

    $ie = New-Object -ComObject "InternetExplorer.application"
    $ie.Visible = $true

    $ie.Navigate($URL)
    #$ie.Navigate("https://google.com")

    while ($ie.Busy -eq $true){
    
                # Wait for the page to load
                Start-Sleep -seconds 5;
    
            }


    write-host "Downloading Friend List"
    $start = Get-Date;
    $VerticalScroll = 0
    $BottomReached = 0
    While($BottomReached -eq 0) {


        $res = $ie.Document.parentWindow.scrollTo(0,$VerticalScroll)
        $VerticalScroll = $VerticalScroll + 100
   
    
        [array]$CheckBottomReached = $ie.Document.IHTMLDocument3_getElementsByTagName('h3') | ? {$_.className -eq "uiHeaderTitle"} | ? {$_.outerHTML -like "*More About*"}
        $BottomReached = $CheckBottomReached.length
    }


        $Friends = $ie.Document.IHTMLDocument3_getElementsByTagName('div') | ? {$_.classname -eq "fsl fwb fcb"} 


        $FriendList = @()


        Write-host "Generation report"
        foreach($friend in $Friends){
        
            $info = New-Object -TypeName PSObject

            $UrlAvailable = $Friend.outerHTML -match "www\.facebook\.com\/[a-zA-Z0-9\.]*\?"

            If ($UrlAvailable){
                $URL = "https://$($Matches.values)" -replace '[?]',''
            }

            else{
                $URL = "https://www.facebook.com/notavailable"
            }
        

            $info | Add-Member -type NoteProperty -name "Name"  -value $friend.TextContent
            $info | Add-Member -type NoteProperty -name "URL"  -value $URL

            $FriendList += $info
   
            }

            $Path = [System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::Desktop) + "\Friendsof$of.csv"

            $FriendList | Sort-Object Name | Export-Csv $Path  -NoTypeInformation

            write-host "CSV file has been exported to desktop"

            #$ie.quit()


}





