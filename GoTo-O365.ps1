﻿# Tired of inputting the user creds everytime you login to O365? Hash the password.
# This script will hash the password to a text file and retrieve it everytime you login to O365. 

cd "C:\temp"
$email_un = "admin-jos@contoso.com"

if(!(Test-Path ".\o365_pw.txt")){

    Read-Host "Enter Email Password" -AsSecureString |  ConvertFrom-SecureString | Out-File ".\o365_pw.txt"
    $email_pw = ".\o365_pw.txt"

}

else{

    $email_pw = ".\o365_pw.txt"
}

$my_email_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $email_un, (Get-Content $email_pw | ConvertTo-SecureString)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $my_email_cred -Authentication Basic -AllowRedirection

Import-PSSession $Session -AllowClobber

