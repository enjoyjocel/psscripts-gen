﻿Function Calculate-IP{   
    <#

    .SYNOPSIS
    Calculates IP address information based off the IP information you feed in CIDR format.
    
    .DESCRIPTION
    Calculates IP address information based off the IP information you feed in CIDR format.
    It will output the Address, Netmask, Wildcard, Network ID, Minimum Host IP, Maximum Host IP, Broadcast, 
    Number of hosts, and Class.
    
    .PARAMETER ip
    IP address in CIDR format. 
    
    .EXAMPLE
    Calculate-IP -IP 192.168.89.8/25
    Calculate-IP -IP 192.168.1.9
    
    .NOTES
    Author: Jocel Sabellano
    Date of First Edit: May 15, 2018

    #>

    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]$Ip
    )
    $IPres = (Invoke-RestMethod -Uri https://uploadbeta.com/api/ipcalc/?cached`&s=$ip -Method GET).split("`n")
    $props = [Ordered]@{
        Address = ($IPres[0] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        NetMask = ($IPres[1] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        Wildcard = ($IPres[2] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        Network = ($IPres[4] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}" | select -ExpandProperty matches | select -ExpandProperty value)
        HostMin = ($IPres[5] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        HostMax = ($IPres[6] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        Broadcast = ($IPres[7] | Select-String "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        Hosts = ($IPres[8] | Select-String "\d{1,3}" | select -ExpandProperty matches | select -ExpandProperty value)
        Class = ($IPres[8] | Select-String "Class [A|B|C]" | select -ExpandProperty matches | select -ExpandProperty value)       
    }
    $IpInfo = New-Object -TypeName PSObject -Property $props
    write-output $ipinfo
}


